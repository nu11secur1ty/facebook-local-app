# facebook-local-app
facebook-local-app.md
Choose either option A or option B.

#Option A: Fast way

Facebook App Id: 435522656639081

*(you will be using a pre-configured Facebook app, which only allows requests from `localhost`)*

#Option B: Setup your own Facebook app
Use this options, if you want to run the service from any other domain than localhost

1. Open https://developers.facebook.com/apps
2. Click "Add a new app"
2. Choose "Website"
3. Click through the setup guide
4. In "Dashboard" copy "App ID" to `config/development.json` config
4. Choose "Settings" in left-hand menu
5. App Domains: "`<your app domain>`"
6. Site Url: "`<your site url>`"
